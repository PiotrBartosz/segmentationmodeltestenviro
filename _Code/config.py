import os

### Paths ###
CONFIG_FILES_DIR = "D:\\Idiaga\\Dataset\\Out\\"
IMAGE_PATH="D:\\Idiaga\\Dataset\\Out\\Image3\\"
MASK_PATH="D:\\Idiaga\\Dataset\\Out\\Combine3\\"
MODEL_PATH="checkpoint/checkpoint_sm_15A.zip"
OUTPUT_PATH="output/"

### Dataset settings ###
TRAIN_RATIO=0.8
TEST_IMAGES=4
TRAIN_BATCH_SIZE=1 #TODO 4
VAL_BATCH_SIZE=1

### Model settings ###
EPOCHS=50
LEARNING_RATE = 0.001
INPUT_IMAGE_HEIGHT=512
INPUT_IMAGE_WIDTH=512
NUM_CLASSES=14


DETECTION_THRESHOLD=0.01

TEST_IMAGES_FILENAMES=["8200.jpg", "8535.jpg", "8402.jpg", "8419.jpg", "8430.jpg"]

ID_TO_NAME = {
    0: "Budynek",
    1: "Drzewo",
    2: "Pojazd",
    3: "WieleDrzew",
    4: "CienieDrzew",
    5: "Skladowisko",
    6: "Parking",
    7: "PryzmaZiemi",
    8: "Wykop",
    9: "ZbiornikWodny",
    10: "ParkingPolny",
    11: "Zweryfikowane",
    12: "RozZiemia",
    13: "Zywoplot",
    14: "InneCienie",
    15: "SCHOULDNOT"

}