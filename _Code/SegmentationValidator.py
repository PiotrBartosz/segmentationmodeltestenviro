import json



def load_conig():
	f = open('config.json')
	data = json.load(f)
	f.close()
	return data['config'][0]


def inserter(config):

	template_path = 'template.py'
	template
	with open('template_path', 'r') as file:
		template = file.read()


	template = template.replace('@IMG_PATH@', config['input_image'])
	template = template.replace('@MASK_PATH@', config['input_mask'])
	template = template.replace('@TRAING_RATIO@', config['training_ratio'])
	template = template.replace('@EPOCHS@', config['epoch_count'])
	template = template.replace('@RESOLUTION@', config['resolution'])
	template = template.replace('@@LEARNING_RATE@@', config['learning_rate'])

	template = template.replace('@MODEL@', config['model'])
	template = template.replace('@ENCODER_NAME@', config['encoder'])
	template = template.replace('@ENCODER_WEIGHTS@', config['encoder_weights'])

	template = template.replace('@INPUT_ID@', config['input_chanel'])
	template = template.replace('@OUTPUT_ID@', config['output_chanel'])
	template = template.replace('@PARAMS@', config['params'])

	template = template.replace('@LOSS_FUNC@', config['loss_func'])

	template = template.replace('@SCORE_FILE@', config['score_file'])

	f = open('email.json')
	data = json.load(f)
	f.close()
	

	template = template.replace('@EMAIL@', data['config'][0]['email'])

	return template

def exec_gen_file(gen_file):
	exec_file = open("model.py")
	exec_file.write(gen_file)
	exec(exec_file.read())
	exec_file.close()



def main():
	config = load_conig()
	config = config
	gen_file = inserter(config)
	exec_gen_file(gen_file)
	

if __name__ == '__main__':
	main()